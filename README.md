# project setup

- download both backend and web app projects (I usually work with monorepos, but here I went with separate ones)
- in backend repo create `.env` file. Check out `.env.example` how `.env` should looks like.
- run `npm install` in both repositiories and `npm run start`. Web app will boot on 3006 port

# questions for domain expert:

- Highcharts offers stock chart type with different tools available. I would ask if any of those are usefull for us and should be included.

# obstacles encountered:

- things went smooth for me, I only spend a little more time on backend than I would like as I have not worked with javascript for last 2 years (using TypeScript only) so I am accustomed to TS taking care for me of highlighting some errors I had been doing in JS code :)

- free marketstack account have one year history, so queries with start date like in example (Start Date: 2013-03-20) would still return data from last year.

- I did not manage to spend on that task 4 hours or less. It took me almost 5, not counting writting this readme. I think I spend at least half an hour more on backend than I would like, for reasons I mentioned above.

# comments, thoughts, assumptions:

In general I would say what I have done is something between protoype and MVP. Backend part is probably more complete, what it lacks may be some error handling, passing different server responses in case of failures. Results pagination is missing, if there would be need to query for more than 1000 results, as API returns at most 1000 results per call.

Some kind of user authentication on backend would be required in final product too. Also, I would consider putting such endpoint as a cloud function (AWS lambdas, google cloud functions etc), if project would already use some cloud functionalities.

Nice addition could be also using Tickers endpoint (https://marketstack.com/documentation - tickers section). This endpoint returns Assets symbols, can be used to either validate user input, or to make user input in web app a selector of only those values.

If we move to frontend part, there a lot is simplified now and lacking.

First, I went with material-ui components, as those provide me with nice looking form parts that does not take much time to implement. While material-ui package is big in size, tree shaking ensures that only used components are included in production build. If budget allows and it would make sense to make custom basic components, I would go with styled-components, storybook and there develop custom inputs and other basic components.

Then UI in general is quite simple, there are hardcoded 3 inputs for 3 assets, no flexibility. Free marketstack account would not handle more than 3 assets for last year span anyway. But if we would use paid account, I would rather make an input for user that would be a search field where stocks names could be searched.

Regarding chart, I went with highcharts stocks chart type, but I did not spend time for exploring those extra tools that chart provides. I am not even sure those are working as intended, so this would have to be explored, whether it's usefull for user at all, and if yes to ensure its working.

Form have now only very basic validation (all fields have to be filled). It lacks proper validation with field types data, error handling and displaying error messages to user.

While you wrote "In addition, please note that some requirements are intentionally vague, cause one of the purposes of this task is to verify how a developer could handle such ambiguities." if that would be possible, I would try to "push" client to get some more details regarding UI, use cases. Even some hand drawing of UI could help checking if UI that will be implemented is something that customer expect.

# testing

on backend, assets-helpers.js and all those methods could use unit tests. E2E test for endpoint could be nice too.

on frontend I would use react-testing-library + jest for testing some basic behaviour like proper form validation (when that would be implemented)
