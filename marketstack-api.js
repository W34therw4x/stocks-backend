const got = require("got");

// you need .env file created with approperiate value. Check .env.example
const accessKey = process.env.MARKETSTACK_API_KEY;
const marketstackApiBaseUrl = "http://api.marketstack.com/v1";
const endOfDayEndpoint = `${marketstackApiBaseUrl}/eod`;

/**
 * @param {string} dateFrom - The query start date.
 * @param {Array<string>} symbols - The assets symbols.
 */
const endOfDayData = async (dateFrom, symbols) => {
  const responseLimit = 1000;
  const sortOrder = "ASC";
  if (symbols.length === 0) return { data: [] };
  let response = null;
  try {
    console.log(
      `Calling: ${endOfDayEndpoint}, with start date: ${dateFrom} and symbols: ${symbols.join(
        ", "
      )}.`
    );
    response = await got(
      `${endOfDayEndpoint}?access_key=${accessKey}&date_from=${dateFrom}&sort=${sortOrder}&limit=${responseLimit}&symbols=${symbols.join(
        ","
      )}`
    );
  } catch (e) {
    throw new Error(
      `Retrieve information about assets data from end of day endpoint failed, reason: ${e}, request options: ${e.options}, stack: ${e.stack}`
    );
  }
  const parsedResponse = JSON.parse(response.body);
  return parsedResponse;
};

module.exports = { endOfDayData };
