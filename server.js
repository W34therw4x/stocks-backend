require("dotenv").config();
const express = require("express");
const cors = require("cors");

const { endOfDayData } = require("./marketstack-api");
const {
  aggregateBySymbol,
  aggregateWeekly,
  aggregateStats,
} = require("./assets-helpers");

const app = express();
app.use(cors());
app.use(express.json());
const port = process.env.PORT || 3000;

app.post("/eod", async (req, res) => {
  const dateFrom = req.body.dateFrom;
  const symbols = req.body.symbols;

  let eodResponse;
  try {
    eodResponse = await endOfDayData(dateFrom, symbols);
  } catch (e) {
    console.log(e);
  }

  const eodResponseData = eodResponse.data;
  const eodBySymbol = aggregateBySymbol(eodResponseData);
  const response = {};
  for (const [symbol, assetsDayData] of Object.entries(eodBySymbol)) {
    const eodByWeeks = aggregateWeekly(assetsDayData);
    let stats = [];
    for (const [date, assetsDayData] of Object.entries(eodByWeeks)) {
      console.log(assetsDayData);
      const weeklyStats = aggregateStats(assetsDayData);
      stats.push({ date, ...weeklyStats });
    }
    response[symbol] = stats;
  }

  res.json({ data: response });
});

app.listen(port, () => {
  console.log("RESTful API server started on: " + port);
});
