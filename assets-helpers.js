var dayjs = require("dayjs");
var isoWeek = require("dayjs/plugin/isoWeek");
dayjs.extend(isoWeek);

/**
 * @typedef {Object} AssetDayData
 * @property {number} open - The value of open index
 * @property {number} high - The value of high index
 * @property {number} low - The value of low index
 * @property {number} close - The value of close index
 * @property {string} date - The date of entry
 * ... there are more properties
 */

/**
 * @param {Array<AssetDayData>} assetsDayData
 */
const aggregateBySymbol = (assetsDayData) =>
  assetsDayData.reduce((acc, assetDayData) => {
    const assetSymbol = assetDayData.symbol;
    if (!acc[assetSymbol]) {
      acc[assetSymbol] = [];
    }
    acc[assetSymbol].push(assetDayData);
    return acc;
  }, {});

/**
 * @param {Array<AssetDayData>} assetsDayData
 */
const aggregateWeekly = (assetsDayData) =>
  assetsDayData.reduce((acc, assetDayData) => {
    const date = dayjs(assetDayData.date).isoWeekday(5).format("YYYY-MM-DD");

    if (!acc[date]) {
      acc[date] = [];
    }

    acc[date].push(assetDayData);

    return acc;
  }, {});

/**
 * @param {Array<AssetDayData>} assetsDayData
 */
const aggregateStats = (assetsDayData) => {
  const stats = assetsDayData.reduce(
    (acc, assetDayData) => {
      acc.open = acc.open + assetDayData.open;
      acc.high = acc.high + assetDayData.high;
      acc.low = acc.low + assetDayData.low;
      acc.close = acc.close + assetDayData.close;
      return acc;
    },
    { open: 0, high: 0, low: 0, close: 0 }
  );
  stats.open = Math.round((stats.open / assetsDayData.length) * 100) / 100;
  stats.high = Math.round((stats.high / assetsDayData.length) * 100) / 100;
  stats.low = Math.round((stats.low / assetsDayData.length) * 100) / 100;
  stats.close = Math.round((stats.close / assetsDayData.length) * 100) / 100;

  return stats;
};

module.exports = {
  aggregateWeekly,
  aggregateBySymbol,
  aggregateStats,
};
